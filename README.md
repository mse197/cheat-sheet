# **This is the cheat-sheet repository** #


## Please provide command line information that others in the class may access.

## Command line information may be for:
* Vim
* BASH
* Git
* Python
* Any other software we will be utilizing during the semester.


### How do I get set up? ###

* Pull the cheat-sheet
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact